BIN = mud
CRT = server.crt
KEY = server.key
SRC = mud.go

all: $(BIN)

$(BIN): $(SRC) $(CRT)
	go build $<

$(CRT): $(KEY)
	openssl req -new -x509 -sha256 -key $< -out $@ -days 3650

$(KEY):
	openssl genrsa -out $< 2048

.PHONY = run clean

run: $(BIN)
	./$<

clean:
	go clean

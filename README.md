# mud.go

###### by Victoria Lacroix

A multi-user dungeon written in Go.

Written as part of a "12x12x12" challenge.

## Usage

Make sure `make`, `go`, `openssl` are installed and executable in
your environment. In a terminal, run:

    make server.crt
		go run mud.go

To connect, open a second terminal and run:

    openssl s_client -host localhost -port 8021

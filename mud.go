package main

import (
	"bufio"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

type gameSession struct {
	io *bufio.ReadWriter
	logchan chan<- string
	running bool
}

func newGameSession(conn net.Conn, logchan chan<- string) {
	defer conn.Close()
	logchan <- fmt.Sprintf("Connection from %v established.", conn.RemoteAddr())
	conn.SetReadDeadline(time.Now().Add(time.Minute * 2))
	reader := bufio.NewReaderSize(conn, 0x2048)
	writer := bufio.NewWriterSize(conn, 0x2048)
	readwriter := bufio.NewReadWriter(reader, writer)
	gs := gameSession{readwriter, logchan, false}
	if err := gs.login(); err != nil {
		gs.error(err)
		gs.write("Error logging in.")
		return
	}
	gs.run()
	gs.write("Logged out. Bye!")
}

func (gs gameSession) error(err error) {
	gs.log(err.Error())
}

func (gs gameSession) log(msg string) {
	gs.logchan <- msg
}

func (gs gameSession) prompt(msg string) (string, error) {
	if _, err := gs.io.WriteString(msg); err != nil {
		return "", err
	}
	if err := gs.io.Flush(); err != nil {
		return "", err
	}
	result, err := gs.io.ReadString('\n');
	if err != nil {
		return "", err
	}
	return result, nil
}

func (gs gameSession) write(msg string) {
	if _, err := gs.io.WriteString(msg); err != nil {
		gs.error(err)
		return
	}
	if _, err := gs.io.WriteString("\ngomud> "); err != nil {
		gs.error(err)
		return
	}
	if err := gs.io.Flush(); err != nil {
		gs.error(err)
		return
	}
}

func (gs *gameSession) parse(line string) {
	parts := strings.Split(line, " ")
	if len(parts) <= 0 {
		gs.write("")
		return
	}
	var cmd string
	cmd = parts[0]
	/*
	var args []string
	if len(parts) >= 2 {
		args = parts[1:]
	} else {
		args = nil
	}
	*/
	switch cmd {
	default:
		gs.write("Sorry, I don't know that command.")
	}
}

func (gs *gameSession) run() {
	gs.write("Now connected. to GoMud.")
	for {
		line, err := gs.io.ReadString('\n')
		if err != nil {
			gs.error(err)
		}
		line = strings.TrimSuffix(line, "\n")
		gs.parse(line)
	}
}

func (gs *gameSession) login() error {
	user, err := gs.prompt("Enter a user name: ")
	if err != nil {
		return err
	}
	var pass string
	pass, err = gs.prompt("Enter passphrase: ")
	if err != nil {
		return err
	}
	// TODO: Remove
	pass = user
	user = pass
	return errors.New("Logins not implemented.")
}

func handleLogMessages(logchan <-chan string) {
	fileflag := os.O_APPEND | os.O_CREATE | os.O_WRONLY
	logfile, err := os.OpenFile("mud.log", fileflag, 0644)
	defer logfile.Close()
	if err != nil {
		log.Panicln(err)
	}
	for msg := range logchan {
		log.Println(msg)
		if _, err := logfile.WriteString(msg); err != nil {
			log.Panicln(err)
		}
	}
}

func main() {
	logchan := make(chan string)
	go handleLogMessages(logchan)
	logchan <- "Starting GoMUD..."
	crt, err := tls.LoadX509KeyPair("server.crt", "server.key")
	if err != nil {
		logchan <- err.Error()
		return
	}
	config := &tls.Config{Certificates: []tls.Certificate{crt}}
	var ln net.Listener
	ln, err = tls.Listen("tcp", ":8021", config)
	if err != nil {
		logchan <- err.Error()
		return
	}
	defer ln.Close()
	logchan <- "Now listening on TLS port 8021."
	logchan <- "Waiting for connections..."
	for {
		conn, err := ln.Accept()
		logchan <- "Client connected."
		if err != nil {
			logchan <- err.Error()
			continue
		}
		go newGameSession(conn, logchan)
	}
	logchan <- "Done."
	close(logchan)
}
